# go_study_runoob



### go和java区别



#### go 项目里面所有代码里面只能由一个main方法？

在Go语言中，一个Go项目可以有多个包（package），但每个可执行文件（如命令行应用程序）必须由一个包含`main`函数的包来构建。`main`函数是Go程序的入口点，每个可执行文件只能有一个入口点。

但是，这并不意味着整个项目里只能有一个`main`方法。实际上，你可以在一个项目中定义多个包含`main`函数的包，每个包都可以编译成独立的可执行文件。这通常用于构建多个独立的命令行工具或应用程序。

例如，你可能有一个项目结构如下：

```
myproject/  
    cmd/  
        tool1/  
            main.go  # 包含tool1的main函数  
        tool2/  
            main.go  # 包含tool2的main函数  
    pkg/  
        common/  
            utils.go  # 包含一些公用的函数或类型
```

在这个结构中，`cmd/tool1/main.go`和`cmd/tool2/main.go`都包含了各自的`main`函数，并且它们都可以独立编译成可执行文件。而`pkg/common/utils.go`包含了一些在两个工具中都可以使用的公用代码。

要编译这些工具，你可以进入每个包含`main`函数的包的目录，并运行`go build`命令。例如：

```bash
cd myproject/cmd/tool1  
go build  
# 这会生成一个名为tool1的可执行文件  
  
cd ../tool2  
go build  
# 这会生成一个名为tool2的可执行文件
```

每个`main`函数都是它所在包的可执行入口点，而不是整个项目的入口点。因此，一个Go项目可以有多个`main`函数，只要它们位于不同的包中。







#### go无法像java那样一个类一个main方法，每个有main方法的类都能运行



确实，Go 语言与 Java 在处理程序的入口点上有显著的不同。在 Java 中，每个包含 `main` 方法的类都可以作为一个独立的程序来运行，因为 Java 虚拟机（JVM）通过类名和 `main` 方法来识别程序的入口点。

然而，在 Go 语言中，整个程序的入口点必须是定义在 `main` 包中的 `main` 函数。这意味着，你不能在一个 Go 项目中拥有多个独立的 `main` 函数，每个都能单独运行，除非这些 `main` 函数位于不同的 `main` 包中，并且每个包都被编译成独立的可执行文件。

Go 的这种设计选择反映了其简洁和一致性的哲学。它强制开发者明确程序的入口点，并确保每个可执行文件都有清晰的起点。





#### go没有Java的继承、多态？

是的，Go语言没有像Java那样的显式继承和传统意义上的多态性。这是Go语言和Java在设计哲学上的一个主要区别。Go语言更注重组合和接口的使用，而不是继承和多态。

在Go语言中，虽然没有继承关键字（extends），但是可以通过嵌入（embedding）结构体来实现类似继承的效果。通过在一个结构体中嵌入另一个结构体，可以复用被嵌入结构体的字段和方法。这种复用方式比传统的继承更加灵活，因为它允许选择性地复用代码，而不是必须继承整个类。

关于多态性，Go语言通过接口（interface）来实现类似的功能。接口定义了一组方法的签名，任何实现了这些方法的类型都被认为实现了该接口。这使得可以在不同的类型之间实现多态行为，只要它们满足相同的接口规范。通过接口，Go语言提供了一种灵活且强大的方式来处理不同类型之间的共同行为。

虽然Go语言没有显式的继承和多态性，但它通过组合和接口的方式提供了一种更加灵活和可扩展的编程模型。这种模型使得代码更加模块化、可复用和可维护，同时也减少了继承带来的复杂性和耦合性。因此，虽然Go语言和Java在这些方面有所不同，但它们都是优秀的编程语言，适用于不同的应用场景和需求。



#### go没有java里面的注解

是的，Go 语言没有像 Java 那样的注解（Annotation）机制。Java 的注解提供了一种元数据的方式，可以在不修改代码的情况下给代码添加额外的信息，这些信息可以在编译时或者运行时被读取和处理。

Go 语言的设计理念强调简单和直接，没有引入注解这样的特性。在 Go 中，如果你想为代码添加额外的信息或元数据，通常的做法是使用结构体、接口或者包级别的变量来存储这些信息。虽然这样做可能比使用注解稍微繁琐一些，但它更加直接和明确，也更容易理解和维护。

另外，Go 的很多库和框架也提供了类似注解的功能，通过特定的命名约定、注释或者其他机制来实现。这些库和框架通常会提供自己的解析和处理机制，以便在运行时读取和使用这些额外的信息。

总的来说，虽然 Go 没有 Java 那样的注解机制，但你可以通过其他方式在 Go 中实现类似的功能。





#### go有没有java的切片Aspect，拦截器，过滤器？

Go 语言并没有直接对应于 Java 中的 AOP（面向切面编程）概念，如 Aspect、拦截器或过滤器。在 Java 中，AOP 通常用于在不修改现有代码的情况下增强功能，如日志记录、事务管理等。AOP 通过拦截器或过滤器在方法调用前后添加额外的逻辑。

然而，Go 语言有自己的特性和机制，可以帮助开发者实现类似的功能。虽然 Go 没有直接支持 AOP 的语言特性，但你可以通过以下方式实现类似的效果：

1. **中间件（Middleware）**：在 Web 开发中，中间件是一种常用的模式，用于在请求和响应之间添加额外的逻辑。例如，在 Go 的 Web 框架（如 Gin、Echo 等）中，你可以使用中间件来实现日志记录、认证、权限控制等功能。
2. **装饰器模式**：装饰器模式是一种结构型设计模式，允许你动态地给一个对象添加一些额外的职责。通过装饰器，你可以在运行时扩展对象的功能，而无需修改其源代码。
3. **钩子函数（Hook Functions）**：在某些 Go 库或框架中，可能会提供钩子函数作为扩展点，允许你在特定的事件发生时执行自定义逻辑。
4. **反射（Reflection）**：Go 的反射包（`reflect`）允许你在运行时检查类型、创建对象、调用方法等。虽然反射在性能上可能不是最优的，但它可以作为一种工具来在运行时动态地操作代码。

虽然这些方法可能没有 Java AOP 那样强大的声明性方式，但它们仍然是在 Go 中实现类似功能的可行方法。重要的是要理解 Go 的哲学和设计原则，以及如何在没有 AOP 的情况下构建可扩展和可维护的代码。

需要注意的是，Go 的简洁性和直接性使得开发者更倾向于在代码中显式地表达逻辑，而不是依赖于像 AOP 这样的高级抽象。这有助于保持代码的清晰性和可维护性。







### go使用redis



参考：https://zhuanlan.zhihu.com/p/637537337

##### **安装**

go-redis 支持 2 个最新的 go 版本且依赖[Go modules](https://link.zhihu.com/?target=https%3A//github.com/golang/go/wiki/Modules)，如果 你还没有 go mod，你需要首先初始化:

```bash
go mod init github.com/my/repo
```



安装 go-redis/**v9** (支持所有的 redis 版本):

这个命令可以在go.mod中导入redis包

```bash
go get github.com/redis/go-redis/v9
```







# go开发web

##### 安装

Gin框架，这个框架据说用起来很好用，具体安装GIN框架方法，可以直接使用Go官方的包管理工具 go get进行安装：

> go get -u github.com/gin-gonic/gin











# go命令



## 一、Go 常用命令

以下是一些常用的Go命令，这些命令可以帮助您在Go开发中进行编译、测试、运行和管理依赖项等任务。

| 命令        | 描述                                              |
| ----------- | ------------------------------------------------- |
| go bug      | 启动一个用于报告bug的工具。                       |
| go build    | 编译Go程序包及其依赖项。                          |
| go clean    | 删除编译生成的对象文件和缓存文件。                |
| go doc      | 显示有关包或符号的文档。                          |
| go env      | 打印有关Go环境的信息。                            |
| go fix      | 更新包以使用新的API。                             |
| go fmt      | 使用gofmt重新格式化Go包的源代码。                 |
| go generate | 通过处理源代码来生成Go文件。                      |
| go get      | 将依赖项添加到当前模块并安装它们。                |
| go install  | 编译并安装包及其依赖项。                          |
| go list     | 列出包或模块的信息。                              |
| go mod      | 用于模块维护,包括初始化模块、添加和更新依赖项等。 |
| go work     | 用于工作区维护,例如查看、清理或打印工作区信息。   |
| go run      | 编译并运行Go程序。                                |
| go test     | 运行包的测试。                                    |
| go tool     | 运行指定的Go工具。                                |
| go version  | 打印Go的版本信息。                                |
| go vet      | 检查 Go 源码并报告可疑的错误。                    |

### 1.1 go build

Go 是强类型编译型语言，因此编译时会将所有依赖编译进同一个二进制文件。

参数介绍

- `-o` 指定输出的文件名，可以带上路径，例如 `go build -o a/b/c`
- `-i` 安装相应的包，编译+`go install`
- `-a` 更新全部已经是最新的包的，但是对标准包不适用
- `-n` 把需要执行的编译命令打印出来，但是不执行，这样就可以很容易的知道底层是如何运行的
- `-p n` 指定可以并行可运行的编译数目，默认是CPU数目
- `-race` 开启编译的时候自动检测数据竞争的情况，目前只支持64位的机器
- `-v` 打印出来正在编译的包名
- `-work` 打印出来编译时候的临时文件夹名称，并且如果已经存在的话就不要删除
- `-x` 打印出来执行的命令，其实就是和`-n`的结果类似，只是这个会执行
- `-ccflags 'arg list'` 传递参数给5c, 6c, 8c 调用
- `-compiler name` 指定相应的编译器，gccgo还是gc
- `-gccgoflags 'arg list'` 传递参数给gccgo编译连接调用
- `-gcflags 'arg list'` 传递参数给5g, 6g, 8g 调用
- `-installsuffix suffix` 为了和默认的安装包区别开来，采用这个前缀来重新安装那些依赖的包，`-race`的时候默认已经是`-installsuffix race`,大家可以通过`-n`命令来验证
- `-ldflags 'flag list'` 传递参数给5l, 6l, 8l 调用
- `-tags 'tag list'` 设置在编译的时候可以适配的那些tag，详细的tag限制参考里面的 Build Constraints

新建 main.go 的源文件,键入下面这些代码:

```go
package main

import "fmt"

func main() {
	fmt.Println("你好, 兰州!")
}
```

打开终端，通过如下命令来编译和运行这个文件了：

```bash
go build main.go
./main
```

#### 1.1.1 指定输出目录

- go build –o [目录]

```bash
mkdir bin # 创建bin 目录
go build -o bin/hello # 编译hello
./bin/hello # 
```

#### 1.1.2 常用环境变量设置编译操作系统和 CPU 架构

- 设置编译操作系统

```bash
# 设置 GOOS 环境变量为 "linux"，指定目标操作系统为 Linux
# 然后使用 go build 编译当前目录中的 Go 代码，并将输出文件命名为 bin/hello
GOOS=linux go build -o bin/hello
./bin/hello 
```

![image-20230922213138895](README.assets/650d973bb07ef24df2263654.png)

- 设置编译CPU架构

```bash
# 设置 GOOS 环境变量为 "linux"，指定目标操作系统为 Linux
# 设置 GOARCH 环境变量为 "amd64"，指定目标 CPU 架构为 64 位 x86
# 然后使用 go build 编译当前目录中的 Go 代码，并将输出文件命名为 bin/hello
GOOS=linux GOARCH=amd64 go build -o bin/hello
```

#### 1.1.3 查看支持的操作系统和CPU架构

要查看Go支持的所有操作系统和CPU架构的列表，您可以查看Go源代码中的 `syslist.go` 文件。这个文件位于 `$GOROOT/src/go/build/syslist.go`。下面是一个示例命令，用于查看支持列表：

```bash
cat $GOROOT/src/go/build/syslist.go
```

这将打印出Go支持的操作系统和CPU架构的完整列表。

通过这些示例，您可以了解如何在Go中指定输出目录和设置编译操作系统和CPU架构。这些功能非常有用，特别是在需要交叉编译或控制输出位置时。

### 1.2 go test

`go test` 命令用于运行Go程序包中的测试。Go的测试是通过在与被测试的代码文件相同目录下的 `_test.go` 文件中编写测试函数来完成的。这里介绍几个常用的参数：

- `-bench regexp` 执行相应的benchmarks，例如 `-bench=.`
- `-cover` 开启测试覆盖率
- `-run regexp` 只运行regexp匹配的函数，例如 `-run=Array` 那么就执行包含有Array开头的函数
- `-v` 显示测试的详细命令

以下是使用 `go test` 命令的基本用法：

1. 进入包含要测试的Go代码的目录。
2. 确保在该目录下有一个或多个 `_test.go` 文件，其中包含测试函数。测试函数的命名必须以 `Test` 开头，后跟被测试函数的名称，并接受一个名为 `t *testing.T` 的参数。
3. 运行以下命令来执行测试：

```bash
go test
```

`go test` 将自动查找并运行当前目录和子目录中的所有测试文件，并输出测试结果。它将显示通过的测试数量、失败的测试数量以及测试覆盖率等信息。

如果您想只运行特定的测试文件或测试函数，可以在 `go test` 后面提供测试文件或测试函数的名称。例如，要运行名为 `TestMyFunction` 的测试函数，可以执行以下命令：

```bash
go test -run TestMyFunction
```

`go test` 还支持许多其他标志和选项，用于控制测试的行为，例如覆盖率分析、并行测试等。您可以使用 `go help test` 命令查看完整的 `go test` 文档以获取更多详细信息。

这里简单举个栗子, 新建 main_test.go 的源文件,键入下面这些代码:

```go
package main

import (
	"fmt"
	"testing"
)

func add(a, b int) int {
	return a + b
}
func TestIncrease(t *testing.T) {
	t.Log("Start testing")
	result := add(1, 2)
	if result == 3 {
		fmt.Println("OK")
	} else {
		fmt.Println("有问题!")
	}
}
```

这里我们队add 这个函数进行测试,验证输出结果,运行如下命令进行测试:

```go
# 运行当前目录及其子目录下的所有测试文件中的测试函数
go test .

# 运行指定名称的测试函数（例如，TestIncrease）
go test -run TestIncrease
```

![image-20230922221352675](README.assets/650da120ff4581f0f8aff9f8.png)

go test ./… 运行测试 go test命令扫描所有*_test.go为结尾的文件，**惯例是将测试代码与正式代码放在同目录（同一个包）**， 如 foo.go 的测试代码一般写在 foo_test.go

### 1.3 go vet

`go vet` 是 Go 编程语言的一个静态分析工具，用于检查 Go 代码中可能包含的潜在错误、不规范的代码和常见问题。它的主要目标是帮助开发人员识别和修复可能导致程序运行时错误的问题，以提高代码的质量和可靠性。

- **-all**: 执行所有可用的 `go vet` 检查。默认情况下，`go vet` 只运行一些常见的检查，但使用 `-all` 标志可以启用所有检查。
- **-shadow**: 检查代码中的变量阴影问题。这个标志用于检测局部变量覆盖外部变量的情况。
- **-printfuncs list**: 自定义 `Printf` 样式函数的检查。您可以指定一个逗号分隔的函数列表，`go vet` 将检查是否正确使用了这些函数来格式化字符串。
- **-composites**: 检查使用复合文字（composite literals）时的问题。这个标志用于检测复合文字的使用是否符合规范。
- **-copylocks**: 检查复制锁的问题。这个标志用于检测代码中是否存在复制锁，以及它们是否正确使用。
- **-lostcancel**: 检查丢失的上下文取消问题。这个标志用于检测代码中是否存在未正确处理的上下文取消。
- **-methods**: 检查接口方法问题。这个标志用于检测接口是否被正确实现。
- **-printf**: 检查格式化字符串问题。这个标志用于检测 `Printf` 样式的格式化字符串是否正确匹配参数。
- **-unreachable**: 检查不可达代码问题。这个标志用于检测不会被执行的代码块。
- **-shadowstrict**: 启用更严格的变量阴影检查。这个标志用于检测更多的变量阴影情况。

举个简单例子,比如printf调用中的参数不匹配:

```go
package main

import (
	"fmt"
)

func main() {
	name := "testing"
	fmt.Printf("%d\n", name)
	fmt.Printf("%s\n", name, name)
}
```

运行命令:

```go
go vet  
```

![image-20230922222809096](README.assets/650da479dda3fab122748a2a.png)

### 1.4 go clean

用于清理构建过程中生成的临时文件和构建缓存。它有助于确保项目处于干净的状态，删除构建过程中生成的中间文件，以便重新构建项目时不会受到旧文件的影响。

参数介绍

- `-i` 清除关联的安装的包和可运行文件，也就是通过go install安装的文件
- `-n` 把需要执行的清除命令打印出来，但是不执行，这样就可以很容易的知道底层是如何运行的
- `-r` 循环的清除在import中引入的包
- `-x` 打印出来执行的详细命令，显示 `go clean` 执行的每个步骤。
- **`go clean -testcache`**: 使用 `-testcache` 标志来清除测试缓存。这将删除与测试相关的缓存文件。
- **`go clean -modcache`**: 使用 `-modcache` 标志来清除模块缓存。这将删除模块依赖项的缓存文件。

### 1.5 go fmt

用于格式化源代码文件，以确保它们符合 Go 语言的编码规范和格式化约定。`go fmt` 命令的主要目的是使代码在不同项目和团队中保持一致的风格，提高代码的可读性和可维护性。

参数介绍

- `-l` 显示那些需要格式化的文件
- `-w` 把改写后的内容直接写入到文件中，而不是作为结果打印到标准输出。
- `-r` 添加形如“a[b:len(a)] -> a[b:]”的重写规则，方便做批量替换
- `-s` 简化文件中的代码
- `-d` 显示格式化前后的diff而不是写入文件，默认是false
- `-e` 打印所有的语法错误到标准输出。如果不使用此标记，则只会打印不同行的前10个错误。
- `-cpuprofile` 支持调试模式，写入相应的cpufile到指定的文件

### 1.6 go get

用于下载、安装和更新 Go 语言包（也称为模块）以及它们的依赖项。这个命令通常用于获取外部包。

参数介绍：

- `-d` 只下载不安装
- `-f` 只有在包含了`-u`参数的时候才有效，不让`-u`去验证import中的每一个都已经获取了，这对于本地fork的包特别有用
- `-fix` 在获取源码之后先运行fix，然后再去做其他的事情
- `-t` 同时也下载需要为运行测试所需要的包
- `-u` 强制使用网络去更新包和它的依赖包
- `-v` 显示执行的命令

看一个实际的例子：

在本地通过源码安装 Go 的调试器 Delve，可以这么做：

```bash
go get github.com/go-delve/delve/cmd/dlv
```

### 1.7 go install

用于编译和安装 Go 包或程序。当你运行 `go install` 时，它会编译当前目录中的 Go 代码，并将生成的二进制可执行文件放置在你的 Go 工作空间的 `bin` 目录中（通常位于 `GOPATH` 中）。

参数介绍：

- `-i`:安装到`GOBIN`环境变量指定的目录,默认为`GOPATH/bin`。
- `-n`:打印将要执行的命令,但不执行。
- `-v`:显示执行的命令。
- `-x`:打印执行的命令及其参数。
- `-work`:打印临时工作目录的名称,然后退出。
- `-pcgo`:针对cgo enabled的包,调用gcc来编译。
- `-pkgdir`:安装完成后的包文件存放目录。
- `-tags`:构建标签,用于选择性地编译相应的代码。
- `-trimpath`:移除文件名中的GOPATH路径前缀。
- `-mod`:模块下载和解析模式,可设为mod、vendor等。

### 1.8 go tool

用于运行各种Go语言工具,主要的参数包括:

- `-n`:打印将要执行的命令,但不执行。
- `-x`:打印执行的命令及其参数。
- `-V`:打印go tool和执行命令的版本信息。
- `-e`:只有在命令执行失败时才打印输出。
- `-json`:以JSON格式输出结果。

常见的子命令及其用途:

- `cover`:测试覆盖率分析工具。
- `fix`:代码迁移工具,可以自动更新旧代码到新语法。
- `fmt`:格式化Go代码的工具。
- `vet`:源码检查工具,用于发现代码错误。
- `doc`:显示包的文档。
- `pprof`:分析和查看分析数据。
- `trace`:编译时跟踪工具。
- `link`:链接器工具,查看二进制文件依赖。
- `addr2line`:转换地址为文件/行号。
- `api`:Go API处理工具。

### 1.9 go generate

这个命令是从Go1.4开始才设计的，用于在编译前自动化生成某类代码。`go generate`和`go build`是完全不一样的命令，通过分析源码中特殊的注释，然后执行相应的命令。这些命令都是很明确的，没有任何的依赖在里面。而且大家在用这个之前心里面一定要有一个理念，这个`go generate`是给开发者用的，不是给使用这个包的人用的，是方便生成一些代码的。

举一个简单的例子，例如经常会使用`yacc`来生成代码，那么常用这样的命令：

```bash
go tool yacc -o gopher.go -p parser gopher.y
```

-o 指定了输出的文件名， -p指定了package的名称，这是一个单独的命令，如果想让`go generate`来触发这个命令，那么就可以在当前目录的任意一个`xxx.go`文件里面的任意位置增加一行如下的注释：

```bash
//go:generate go tool yacc -o gopher.go -p parser gopher.y
```

这里注意了，`//go:generate`是没有任何空格的，这其实就是一个固定的格式，在扫描源码文件的时候就是根据这个来判断的。

所以可以通过如下的命令来生成，编译，测试。如果`gopher.y`文件有修改，那么就重新执行`go generate`重新生成文件就好。

```bash
$ go generate
$ go build
$ go test
```

### 1.10 godoc

在Go1.2版本之前还支持`go doc`命令，但是之后全部移到了godoc这个命令下，需要这样安装`go get golang.org/x/tools/cmd/godoc`

很多人说go不需要任何的第三方文档，例如chm手册之类的，因为它内部就有一个很强大的文档工具。

如何查看相应package的文档呢？

例如builtin包，那么执行`godoc builtin`

如果是http包，那么执行`godoc net/http`

查看某一个包里面的函数，那么执行`godoc fmt Printf`

也可以查看相应的代码，执行`godoc -src fmt Printf`

通过命令在命令行执行 godoc -http=:端口号 比如`godoc -http=:8080`。然后在浏览器中打开`127.0.0.1:8080`，将会看到一个golang.org的本地copy版本，通过它可以查询pkg文档等其它内容。如果设置了GOPATH，在pkg分类下，不但会列出标准包的文档，还会列出本地`GOPATH`中所有项目的相关文档，这对于经常被墙的用户来说是一个不错的选择。

### 1.11 go run

用于编译并运行 Go 源代码文件。它是一个方便的工具，可用于在不需要显式构建可执行文件的情况下直接运行 Go 程序。常用的参数包括:

- `-n`:打印执行的命令,但不执行。
- `-x`:打印执行的命令及参数。
- `-race`:启用数据竞争检测。
- `-gcflags`:传递参数给编译器,如优化级别等。
- `-buildmode`:指定编译模式,如共享库或插件等。
- `-ldflags`:传递参数给链接器。
- `-trimpath`:去除输出中的文件路径信息。
- `-memprofile`:写入内存概要文件。
- `-cpuprofile`:写入CPU概要文件。
- `-blockprofile`:写入阻塞概要文件。
- `-timeout`:执行超时时间。
- `-args`:传递参数给程序,放在最后。

例如:

```bash
go run -race -ldflags "-s -w" main.go args
```

此命令会启用竞争检测和移除调试信息,并传递args参数给main.go执行。

分享是一种快乐，开心是一种态度！



本文作者：贾维斯Echo

本文链接：https://www.cnblogs.com/taoxiaoxin/p/17747338.html

版权声明：本作品采用知识共享署名-非商业性使用-禁止演绎 2.5 中国大陆[许可协议](https://www.cnblogs.com/taoxiaoxin/p/17747338.html)进行许可。





















# go mod命令

https://blog.csdn.net/kgs0716/article/details/130686880

```
#初始化模块,导入项目不需要这个
go mod init <项目模块名称>
go mod edit:编辑go.mod文件，选项有-json、-require和-exclude，可以使用帮助go help mod edit
go mod graph:以文本模式打印模块需求图

#依赖关系处理 ,根据go.mod文件  添加缺少的包，且删除无用的包
go mod tidy

将依赖包复制到项目下的 vendor目录。  
go mod vendor 

# 如果包被屏蔽(墙),可以使用这个命令，随后使用go build -mod=vendor编译


#验证依赖是否正确  校验模块是否被篡改过
go mod verify 

#更换下载源
go env -w GOPROXY=https://goproxy.cn,direct

#查找依赖
go mod why 

#显示依赖关系
go list -m all 

#显示详细依赖关系
go list -m -json all 

#下载依赖
go mod download [path@version]
```


