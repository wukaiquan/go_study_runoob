package mysql

import "database/sql"

type Fiction struct {
	ID   int
	Name string
	Auth sql.NullString //用sql.NullString处理可能为空的字符串
}
