package mysql

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

/*
*函数名如果以小写字母开头，则它是未导出的（unexported），意味着它只能在定义它的包内部被访问
改成大写开头，main.go里面可以运行
*/
func Connect() (*sql.DB, error) {

	// 构建连接字符串：用户名:密码@协议(地址)/数据库名
	dsn := "root:123456@tcp(127.0.0.1:3306)/book"
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		log.Fatal(err)
	}
	//defer db.Close() //关闭Mysql连接

	// 测试连接
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to the database successfully!")
	return db, err
}

// 关闭Mysql连接
func CloseDB(db *sql.DB) {
	defer db.Close() //关闭Mysql连接
}
