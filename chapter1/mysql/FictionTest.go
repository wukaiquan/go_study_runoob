package mysql

import "fmt"

/*
*
值传递
*/
func TestFiction(f Fiction) {
	fmt.Println(f.Name)
}

/*值传递，传递的是副本*/
func TestFiction2(fiction *Fiction) {

	fmt.Println(fiction.Name)
}

/*引用传递,传递进来的结构体值改了，应为传的是指针，通过指针修改了地址*/
func TestFiction3(fiction *Fiction) {
	fiction.Name = "蚂蚱"
}

/*值传递，传递的是副本*/
func TestFiction4(fiction Fiction) {
	fiction.Name = "寒冰"
}
