package main

import "fmt"

/**语言范围(Range) */

var pow = []int{1, 2, 4, 8, 16, 32, 64, 128, 256}

func main13() {

	for i, v := range pow {
		fmt.Printf("2**%d = %d \n", i, v)
	}

	//Map(集合)
	map1 := make(map[int]float32)
	map1[1] = 1.0
	map1[2] = 2.0
	map1[3] = 4.0
	map1[4] = 4.0
	for key, value := range map1 {
		fmt.Printf("key is %d - value is : %f \n", key, value)
	}

	for key := range map1 {
		fmt.Printf("key is %d \n", key)
	}
	for _, value := range map1 {
		fmt.Printf("value is %f \n", value)
	}

	var siteMap map[string]string
	siteMap = make(map[string]string)
	siteMap["google"] = "谷歌"
	siteMap["runoob"] = "菜鸟教程"
	siteMap["baidu"] = "百度"
	siteMap["wiki"] = "维基百科"
	for site := range siteMap {
		fmt.Println(site, "中文是", siteMap[site])
	}
	delete(siteMap, "wiki") //删除元素
	fmt.Println(len(siteMap))

	//这是我们使用 range 去求一个 slice 的和
	nums := []int{1, 2, 3, 4}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println("sum: ", sum)
}
