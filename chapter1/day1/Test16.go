package main

import "fmt"

/**Go 语言通过内置的错误接口提供了非常简单的错误处理机制。*/
type DivideError struct {
	dividee int
	divider int
}

func (de *DivideError) Error() string {
	strFormat := `connot proceed,the divider is zero. dividee: %d divider: 0`
	return fmt.Sprintf(strFormat, de.divider)
}

func Divide(varDividee int, varDivider int) (result int, errorMsg string) {
	if varDivider == 0 {
		dData := DivideError{
			dividee: varDividee,
			divider: varDivider,
		}
		errorMsg = dData.Error()
		return
	} else {
		return varDividee / varDivider, ""
	}
}

func main16() {

	//正常情况， go的if语句可以2个条件，;分开
	if result, errorMsg := Divide(100, 10); errorMsg == "" {
		fmt.Println("100/10=", result)
	}
	if _, errorMsg := Divide(100, 0); errorMsg != "" {
		fmt.Println("errorMsg is :", errorMsg)
	}
}
