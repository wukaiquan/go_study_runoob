package main

import "fmt"

/*参考: https://www.runoob.com/go/go-operators.html
*常量还可以用作枚举：
 */
const (
	Unknown = 0
	Female  = 1
	Male    = 2
)

/*
*
const 常量使用
*/
func main5() {

	const LENGTH int = 10
	const HIGH int = 20
	var a, b, c = 1, false, "go"

	var area int
	area = LENGTH * HIGH
	fmt.Println(area)
	fmt.Println(a, b, c)
	fmt.Println(Unknown, Female, Male)

	/**iota，特殊常量，可以认为是一个可以被编译器修改的常量。*/
	const (
		a1 = iota //0
		b1        //1
		c1        //2
		d  = "ha" //独立值，iota += 1
		e         //"ha"   iota += 1
		f  = 100  //iota +=1
		g         //100  iota +=1
		h  = iota //7,恢复计数
		i         //8
	)
	fmt.Println(a1, b1, c1, d, e, f, g, h, i)

	/**iota 表示从 0 开始自动加 1，所以 i=1<<0, j=3<<1（<< 表示左移的意思），即：i=1, j=6，这没问题，关键在 k 和 l，从输出结果看 k=3<<2，l=3<<3。*/
	const (
		i1 = 1 << iota
		j  = 3 << iota //0011  左移1位 0110 6
		k              //0011  左移2位 1100 12
		l              //0000 0011 左移3位  0001 1000  24
	)

	fmt.Println("i1=", i1)
	fmt.Println("j=", j)
	fmt.Println("k=", k)
	fmt.Println("l=", l)
}
