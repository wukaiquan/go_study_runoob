package main

import "fmt"

/**结构体，Java里面的实体类*/
type Books struct {
	title   string
	author  string
	subject string
	book_id int
}

func main11() {

	book := Books{"go语言", "www.baidu.com", "go语言教程", 561}
	var book2 = Books{title: "java语言", author: "www.baidu.com", subject: "java教程"}
	var book3 = Books{book_id: 123}
	fmt.Println(book)
	fmt.Println(book2)
	fmt.Println(book3)

	//访问结构体成员
	fmt.Println(book.title)
	fmt.Println(book2.title)

	printBook(&book)
}

// 结构体指针
func printBook(book *Books) {
	fmt.Println(book.title)
	fmt.Println(book.author)
}
