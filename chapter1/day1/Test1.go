package main

import (
	"fmt"
)

/*
* 参考: https://www.runoob.com/go/go-operators.html
报名必须是main,main方法才能运行
没有分号，换行就是一行代码
声明的变量必须要用，不然报错 .\Test1.go:16:6: code2 declared and not used
一个包下的所有类里面只能有一个main()方法，
*/
func main1() {
	//fmt.Println("hello go")

	//var code2 = 123
	var code = 123
	var date = "2020-12-31"
	var url = "Code=%d&endDate=%s"
	var target_url = fmt.Sprintf(url, code, date)
	fmt.Println(target_url)
}
