package main

import (
	"fmt"
	"strconv"
)

/**类型转换*/
func main14() {

	var a int = 10
	var c = 20
	var b float64 = float64(a)
	var mean float32
	mean = float32(c) / float32(a)
	fmt.Printf("mean的值为: %f    b的值为: %f \n", mean, b)

	str := "123"
	num, err := strconv.Atoi(str)
	if err != nil {
		fmt.Println("转换错误", err)
	} else {
		fmt.Printf("字符串 ‘%s’ 转为整数：%d \n", str, num)
	}

	e := 3.14159
	d := strconv.FormatFloat(e, 'f', 2, 64)
	fmt.Printf("浮点数%f 转为字符串为: '%s' \n", e, d)

	//接口类型转换
	// 接口类型转换有两种情况：类型断言和类型转换。
	// 类型断言
	var i interface{} = "hello,word"
	stri, ok := i.(string) //类型断言
	if ok {
		fmt.Printf("'%s' is a string\n", stri)
	} else {
		fmt.Printf("convrsion faild")
	}

	var w Writer = &StringWriter{} //w为 stringWriter接口提的指针
	sw := w.(*StringWriter)        //类型转换
	sw.str = "hello go"
	fmt.Println(sw.str)
}

type Writer interface {
	Writer([]byte) (int, error)
}
type StringWriter struct {
	str string
}

/*
*
func: 关键字表示定义一个新的函数。
(sw *StringWriter): 这部分表示这是一个方法，而不是一个普通的函数。sw是接收者（receiver），它的类型是*StringWriter，即指向StringWriter结构体的指针。方法可以通过接收者来访问和修改接收者类型的字段。
Writer(data []byte): 这是方法的名称和参数列表。Writer是方法名，data是方法的唯一参数，类型为[]byte，即字节切片。
(int, error): 这是方法的返回类型列表。方法返回两个值：一个int类型的整数值和一个error接口类型的值。在Go中，返回error是一种常用的错误处理机制。如果方法执行过程中发生错误，可以通过返回非nil的error值来通知调用者。
*/
func (sw *StringWriter) Writer(data []byte) (int, error) {
	sw.str += string(data)
	return len(data), nil
}
