package main

import (
	"fmt"
)

/*
*
全局变量
*/
var x, y int
var ( // 这种因式分解关键字的写法一般用于声明全局变量
	a  int
	b1 bool
)

/**方法里面局部变量*/
func main4() {
	var i int     //默认0
	var f float64 //默认0
	var b bool    //默认false
	var s string  //默认""
	fmt.Printf("%v %v %v %q\n", i, f, b, s)

	var d = true // var声明变量
	fmt.Println(d)

	e := "hello" //:= 声明变量
	fmt.Println(e)

	var name1, name2, name3 = 1, "hello", true //自动判断类型
	fmt.Println(name1, name2, name3)

	fmt.Println(a, b1, x, y)
}
