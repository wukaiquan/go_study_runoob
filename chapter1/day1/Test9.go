package main

import "fmt"

/**数组*/
var balance [10]float32
var balance2 = [...]float32{1000.0, 2.0, 3.4, 7.0, 50.0}
var balance3 = [5]float32{1000.0, 2.0, 3.4, 7.0, 50.0}

func main9() {
	balance4 := [...]float32{1000.0, 2.0, 3.4, 7.0, 50.0}

	var i int
	for i = 0; i < len(balance2); i++ {
		fmt.Println(balance2[i])
	}

	for i = 0; i < len(balance4); i++ {
		fmt.Println(balance4[i])
	}
}
