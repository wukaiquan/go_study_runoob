package main

import (
	"fmt"
	"math/rand"
)

/*
*
条件语句
*/
func main7() {

	a := true
	if a {
		fmt.Println("haha")
	} else {
		fmt.Println("hehe")
	}
	switch a {
	case true:
		fmt.Println("haha1")
		break
	case false:
		fmt.Println("haha1")
		break
	}

	//Go 的通道有两种操作方式，一种是带 range 子句的 for 语句，另一种则是 select 语句，它是专门为了操作通道而存在的。这里主要介绍 select 的用法。
	// 准备好几个通道。
	intChannels := [5]chan int{
		make(chan int, 1),
		make(chan int, 1),
		make(chan int, 1),
		make(chan int, 1),
		make(chan int, 1),
	}
	// 随机选择一个通道，并向它发送元素值。
	index := rand.Intn(5)
	fmt.Printf("The index: %d\n", index)
	intChannels[index] <- index
	// 哪一个通道中有可取的元素值，哪个对应的分支就会被执行。
	select {
	case <-intChannels[0]:
		fmt.Println("The first candidate case is selected.")
	case <-intChannels[1]:
		fmt.Println("The second candidate case is selected.")
	case elem := <-intChannels[2]:
		fmt.Printf("The third candidate case is selected. The element is %d.\n", elem)
	default:
		fmt.Println("No candidate case is selected!")
	}
}
