package main

import "fmt"

/*
*数据类型  布尔型，数字类型，字符串
派生类型：指针、数组、结构化类型、Channel类型、函数类型、切片类型（可变数组）、接口类型(interface)、Map类型
苏子类型: unit8 无符号 8 位整型 (0 到 255),unit16 无符号 16 位整型 (0 到 65535) ,uint32 无符号 32 位整型 (0 到 4294967295),uint64 无符号 64 位整型 (0 到 18446744073709551615)
int8 有符号 8 位整型 (-128 到 127) ,int16 有符号 16 位整型 (-32768 到 32767) ,int32 有符号 32 位整型 (-2147483648 到 2147483647)
,int64 有符号 64 位整型 (-9223372036854775808 到 9223372036854775807)
浮点型: float32,float64,complex64 32 位实数和虚数,complex128 64 位实数和虚数
*/
func main2() {

	var a string = "google"
	fmt.Println(a)

	var b, c int = 1, 2
	fmt.Println(b, c)

	var u1 uint8 = 255
	fmt.Println(u1)

	var u2 uint16 = 32767
	fmt.Println(u2)

	var u3 uint32 = 541216546
	fmt.Println(u3)

	var i1 int8 = -128
	var i2 int16 = -32768
	var i3 int32 = 2147483647
	var i4 int64 = 9223372036854775807
	fmt.Println(i1, i2, i3, i4)
}
