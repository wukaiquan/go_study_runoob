package main

import (
	"fmt"
	"strconv"
)

func max(num1, num2 int) int {
	var result int
	if num1 > num2 {
		result = num1
	} else {
		result = num2
	}
	return result
}

// go函数可以返回多个值
func multiFun(a, b, c int) (int, string) {
	//int转字符串
	var str = "str is " + strconv.Itoa(a+c)
	return a * b, str
}

func main8() {
	num1, num2 := 1, 2
	a := max(num1, num2)
	fmt.Println(a)

	var c, d = multiFun(num1, num2, a)
	fmt.Println(c, d)

	//字符串转int
	var num int64 = 1324564654
	var str string = strconv.FormatInt(num, 10) //10代表10进制
	fmt.Println("num=%d,str=%s", num, str)
}
