package main

import "fmt"

const Max = 3

/**指针*/
func main10() {

	var a int = 10
	var d int = 20
	fmt.Println("变量地址:", &a)

	var ip *int
	ip = &a //指针变量的存储地址
	fmt.Printf("a变量的地址：%x\n", &a)
	fmt.Printf("ip 指针地址：%x\n", ip)

	//使用指针访问值
	fmt.Println("*ip变量的值:", *ip)

	//指向指针的指针
	var pp **int
	pp = &ip
	fmt.Println("**pp变量的值:", **pp)

	fmt.Println("-------------------------------------")

	//指针数组
	b := []int{10, 100, 200}
	var i int
	for i = 0; i < Max; i++ {
		//printf里面的%d,%x才有用
		fmt.Printf("b[%d]=%d\n", i, b[i])
	}
	var ptr [3]*int
	for i = 0; i < Max; i++ {
		ptr[i] = &b[i] //循环复制给指针数组
	}

	for i = 0; i < Max; i++ {
		fmt.Printf("b[%d]=%d\n", i, *ptr[i])
	}

	swap(&a, &d)
	fmt.Println("交换后a的值:", a)
	fmt.Println("交换后d的值:", d)
}

// 向函数传递指针
func swap(x *int, y *int) {

	var temp int
	temp = *x
	*x = *y
	*y = temp
}
