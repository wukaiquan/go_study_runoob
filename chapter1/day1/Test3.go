package main

import "fmt"

// for循环
func forFun() {
	/* for 循环 */
	for a < 20 {
		fmt.Printf("a 的值为 : %d\n", a)
		a++
		if a > 15 {
			/* a 大于 15 时使用 break 语句跳出循环 */
			break
		}
	}
}

// continu循环 在变量 a 等于 15 的时候跳过本次循环执行下一次循环：
func continueFun() {
	/* 定义局部变量 */
	var a int = 10

	/* for 循环 */
	for a < 20 {
		if a == 15 {
			/* 跳过此次循环 */
			a = a + 1
			continue
		}
		fmt.Printf("a 的值为 : %d\n", a)
		a++
	}
}

/*
*
在变量 a 等于 15 的时候跳过本次循环并回到循环的开始语句 LOOP 处：
*/
func loopFun() {
	/* 定义局部变量 */
	var a int = 10

	/* 循环 */
LOOP:
	for a < 20 {
		if a == 15 {
			/* 跳过迭代 */
			a = a + 1
			goto LOOP
		}
		fmt.Printf("a的值为 : %d\n", a)
		a++
	}
}
