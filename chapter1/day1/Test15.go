package main

import "fmt"

/**语言接口
Go 语言提供了另外一种数据类型即接口，它把所有的具有共性的方法定义在一起，任何其他类型只要实现了这些方法就是实现了这个接口。

接口可以让我们将不同的类型绑定到一组公共的方法上，从而实现多态和灵活的设计。

Go 语言中的接口是隐式实现的，也就是说，如果一个类型实现了一个接口定义的所有方法，那么它就自动地实现了该接口。因此，我们可以通过将接口作为参数来实现对不同类型的调用，从而实现多态。
*/

type Apple_interface interface {
	eat()
}
type RedApple struct {
}

/**不像Java那样需要用implement关键字，只需要实现方法就行了*/
func (redApple RedApple) eat() {
	fmt.Println("tom eat red apple")
}

type GreenApple struct {
}

func (greenApple GreenApple) eat() {
	fmt.Println("lili eat green apple")
}
func main15() {
	var apple Apple_interface

	apple = new(RedApple)
	apple.eat()

	apple = new(GreenApple)
	apple.eat()

	var s Shape
	s = Rectangle{width: 10, height: 5}
	fmt.Printf("矩形面积: %f \n", s.area())

	s = Circle{radius: 3}
	fmt.Printf("圆形面积: %f \n", s.area())
}

type Shape interface {
	area() float64
}
type Rectangle struct {
	width  float64
	height float64
}

func (r Rectangle) area() float64 {
	return r.width * r.height
}

type Circle struct {
	radius float64
}

/** java 里面 a.run() run()方法没有参数，里面获取不到a,但是go可以，同样入参都是空的，go里面可以用a的属性*/
func (c Circle) area() float64 {
	return 3.14 * c.radius * c.radius
}
