package main

import "fmt"

/*
*Go 数组的长度不可改变，在特定场景中这样的集合就不太适用，Go 中提供了一种灵活，
功能强悍的内置类型切片("动态数组")，与数组相比切片的长度是不固定的，可以追加元素，在追加时可能使切片的容量增大。
*/
func main12() {
	//make() 函数来创建切片:
	var numbers = make([]int, 3, 5)
	printSlice(numbers)

	s := []int{1, 23, 3} //直接初始化切片，[] 表示是切片类型，{1,2,3} 初始化值依次是 1,2,3，其 cap=len=3。
	printSlice(s)

	//空切片
	var n []int
	printSlice(n)

	//切片截取
	n1 := []int{1, 1, 23, 4, 31, 21, 51, 61, 12, 17, 18}
	fmt.Println("n1[1:4] == ", n1[1:4]) //截取1-3
	fmt.Println("n1[:3] == ", n1[:3])   //截取3之前，0-2
	fmt.Println("n1[4:] == ", n1[4:])   //截取4之后，4-x
}

func printSlice(x []int) {
	/**切片是可索引的，并且可以由 len() 方法获取长度。
	切片提供了计算容量的方法 cap() 可以测量切片最长可以达到多少
	*/
	fmt.Printf("len=%d cap=%d slice=%v \n", len(x), cap(x), x)
}
