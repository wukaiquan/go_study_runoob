package web

import "github.com/gin-gonic/gin"

type StructA struct {
	FieldA string `form:"field_a"`
}
type LoginForm struct {
	//必须大写才能在其他方法中用,form写成了from，导致一直绑定不了
	User     string `form:"user" binding:"required"`
	Password string `form:"password" binding:"required"`
}
type StructB struct {
	NestedStruct StructA
	FieldB       string `form:"field_b"`
}

// 模拟一些私人数据
var secrets = gin.H{
	"foo":    gin.H{"email": "foo@bar.com", "phone": "123433"},
	"austin": gin.H{"email": "austin@example.com", "phone": "666"},
	"lena":   gin.H{"email": "lena@guapa.com", "phone": "523443"},
}
