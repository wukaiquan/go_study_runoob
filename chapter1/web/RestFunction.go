package web

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func options(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func head(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func patching(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func deleting(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func putting(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func posting(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func getting(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func readEndpoint(c *gin.Context) {
	var msg struct {
		Name    string `json:"user"`
		Message string
		Number  int
	}
	msg.Name = "Lena"
	msg.Message = "hey"
	msg.Number = 123
	c.JSON(http.StatusOK, msg)
}

func submitEndpoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func loginEndpoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

// curl "http://localhost:8080/getb?field_a=hello&field_b=world"
func GetDataB(c *gin.Context) {
	var b StructB
	c.Bind(&b)
	c.JSON(200, gin.H{
		"a": b.NestedStruct,
		"b": b.FieldB,
	})
}
