package web

/**json传参接收*/
type MyRequest struct {
	Name string `json:"name"  binding:"required"`
	Age  int    `json:"age"`
}

/**get传参接收 注意是from,不是json*/
type MyGETRequest struct {
	Name string `form:"name" binding:"required"` // 注意这里使用的是form而不是json,from:"name"之间不能有空格
	Age  int    `form:"age"`                     // 同理
}

/**json返回*/
type MyResponse struct {
	Message string `json:"message"`
}
