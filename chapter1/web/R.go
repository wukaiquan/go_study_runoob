package web

/**泛型结构体*/
type R[T any] struct {
	code    int
	msg     string
	success bool
	data    T
}
type Page[T any] struct {
	total     int
	totalPage int
	pageSize  int
	pageNum   int
	data      T
}

/*泛型使用*/
type Addable interface {
	Add(Addable) Addable
}
type MyInt int

// 假设您有一个自定义的数值类型 MyInt，它实现了 Addable 接口
/**(a MyInt) 是调用者，不同于Java，go里面的函数调用者也是可以参与计算的，java虽然调用不参与计算，但是调用者里面的属性可以参与计算
list a,list b, a.addAll(b) ,a里面的存储桶参与了计算，a的size = a+b,如果是基础类型，Java是直接+，基础类型是final不能继承

这个函数是MyInt类型的一个方法，名为Add，它接受一个Addable接口类型的参数b，并返回一个Addable接口。这里的Addable是一个假设的接口，它可能定义了一个或多个方法，但在你提供的代码片段中并没有给出其完整的定义。

函数的主体首先尝试将参数b断言为MyInt类型。类型断言在Go中用于检查接口值是否包含特定类型的值，并获取该值（如果可能的话）。b.(MyInt)会尝试将b转换为MyInt类型，如果转换成功，ok将为true，并且other将包含转换后的值。


b.(MyInt) 是一个类型断言，它尝试将接口值 b 断言（或转换）为 MyInt 类型。
这个类型断言返回两个值：
other：如果断言成功，它将是 b 转换为 MyInt 类型后的值。
ok：一个布尔值，表示断言是否成功。如果 b 确实包含了一个 MyInt 类型的值，ok 将为 true；否则，ok 为 false。
*/
func (a MyInt) Add(b Addable) Addable {
	if other, ok := b.(MyInt); ok {
		return MyInt(a) + other
	}
	// 处理类型不匹配的情况
	return nil
}
func Add[T Addable](a T, b T) T {
	return a.Add(b).(T)
}
