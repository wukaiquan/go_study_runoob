package web

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func StartWeb2() {
	// 新建一个没有任何默认中间件的路由
	r := gin.New()

	// 全局中间件
	// Logger 中间件将日志写入 gin.DefaultWriter，即使你将 GIN_MODE 设置为 release。
	// By default gin.DefaultWriter = os.Stdout
	r.Use(gin.Logger())

	// Recovery 中间件会 recover 任何 panic。如果有 panic 的话，会写入 500。
	r.Use(gin.Recovery())

	// 你可以为每个路由添加任意数量的中间件。
	r.GET("/benchmark", MyBenchLogger(), benchEndpoint)

	// 认证路由组
	// authorized := r.Group("/", AuthRequired())
	// 和使用以下两行代码的效果完全一样:
	authorized := r.Group("/")
	// 路由组中间件! 在此例中，我们在 "authorized" 路由组中使用自定义创建的
	// AuthRequired() 中间件
	authorized.Use(AuthRequired())
	{
		authorized.POST("/login", loginEndpoint)
		authorized.POST("/submit", submitEndpoint)
		authorized.POST("/read", readEndpoint)

		// 嵌套路由组
		testing := authorized.Group("testing")
		testing.GET("/analytics", analyticsEndpoint)
	}

	//映射查询字符串或表单参数
	r.POST("/post", func(c *gin.Context) {
		ids := c.QueryMap("ids")        //链接上的参数
		names := c.PostFormMap("names") //form-data传参
		fmt.Printf("ids: %v; names: %v", ids, names)
	})

	// 监听并在 0.0.0.0:8080 上启动服务
	r.Run(":8080")
}

func analyticsEndpoint(c *gin.Context) {

	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

func AuthRequired() gin.HandlerFunc {
	return nil
}

func MyBenchLogger() gin.HandlerFunc {

	return nil
}

func benchEndpoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "success"})
}
